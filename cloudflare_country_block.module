<?php

/**
 * @file
 * Code for the Cloudflare Country Block feature.
 */

define('CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER', 'HTTP_CF_IPCOUNTRY');

/**
 * Implements hook_help().
 */
function cloudflare_country_block_help($path, $arg) {
  switch ($path) {
    case 'admin/help#cloudflare_country_block':

      $filepath = dirname(__FILE__) . '/README.txt';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}

/**
 * Perform country blocking based on settings.
 *
 * Implements hook_init().
 */
function cloudflare_country_block_init() {
  // Ensure that the super administrator has always access no matter what
  // so you can't lock yourself (or drush!) out.
  global $user;
  if (drupal_is_cli() || (is_object($user) && isset($user->uid) && $user->uid == 1)) {
    return;
  }

  // Firstly check if the blocking mechanism is enabled at all.
  if (variable_get('cloudflare_country_block_enabled', FALSE)) {

    $block_access = FALSE;
    $country_code = '';

    // Get country code from Cloudflare request headers.
    if (isset($_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER])) {
      $country_code = $_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER];

      $blocked_countries = variable_get('cloudflare_country_block_countries', array());
      if (array_key_exists($country_code, $blocked_countries)) {
        // This is a match! We're being visited from a country that was selected
        // to be blocked!
        $block_access = TRUE;
      }
    }
    else {
      // We did not get the Cloudflare request header, so the config decides.
      if (variable_get('cloudflare_country_block_cloudflare_missing', 'nothing') == 'block') {
        $block_access = TRUE;
      }
    }

    // Check if the access has to be blocked.
    if ($block_access) {
      // Log blocking if configured.
      if (variable_get('cloudflare_country_block_watchdog', TRUE)) {
        if ($country_code == '') {
          watchdog('cloudflare_country_block', 'Blocked request from IP :ip with an empty country code.', array(':ip' => ip_address()), WATCHDOG_INFO);
        }
        else {
          watchdog('cloudflare_country_block', 'Blocked request from IP :ip with country code :country', array(
            ':ip' => ip_address(),
            ':country' => $country_code,
          ), WATCHDOG_INFO);
        }
      }

      // Pet header code according to config.
      $response_code = variable_get('cloudflare_country_block_response_code', '410');
      module_load_include('inc', 'cloudflare_country_block', 'http_response_codes');

      $response_codes = cloudflare_country_block_get_http_codes();
      header('HTTP/1.0 ' . $response_codes[$response_code]['technical_response']);

      // Performing exit according to chosen method.
      switch (variable_get('cloudflare_country_block_exit_method', 'drupal_exit')) {
        case 'php_exit':
          exit;

        case 'access_denied':
          drupal_access_denied();
          break;

        case 'drupal_exit':
        default:
          drupal_exit();
          // Just to be nice code and prevent copy paste errors.
          break;

      }

    }
  }
}

/**
 * Register the route for the configuration interface.
 *
 * Implements hook_menu().
 */
function cloudflare_country_block_menu() {
  $items = array();
  $items['admin/config/system/cloudflare-country-block'] = array(
    'title' => 'Cloudflare Country Block',
    'description' => 'Configuration for Cloudflare Country Block module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('cloudflare_country_block_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Callback to generate the configuration form.
 *
 * @param array $form
 *   Render array of the form.
 * @param array $form_state
 *   Form State from the Drupal Form API.
 *
 * @return array
 *   The render array for the configuration form
 *
 * @see cloudflare_country_block_menu()
 */
function cloudflare_country_block_form(array $form, array &$form_state) {
  /*
  We have the following configuration options:
  - Is the blocker enabled or disabled?
  - Which countries are being blocked?
  - Which HTTP return code is being given when a country is matching?
  - Do or do not log this to Watchdog
  - Which exit method is preferred?
  - How should the module react if it is enabled and the Cloudflare response
  header is not given?
   */

  // Get the actual cloudflare request value from the request parameters.
  $cloudflare_value = isset($_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER]) ? $_SERVER[CLOUDFLARE_COUNTRY_BLOCK_REQUEST_IDENTIFIER] : FALSE;

  $form['cloudflare_country_block_information_title'] = array(
    '#type' => 'markup',
    '#markup' => t('Cloudflare Request Information'),
    '#prefix' => '<h2>',
    '#suffix' => '</h2>',
    '#weight' => 1,
  );

  $form['cloudflare_country_block_information'] = array(
    '#type' => 'markup',
    '#markup' => ($cloudflare_value === FALSE ?
      t('<p style="color:#CD5C5C">The Cloudflare identifier was not found in the request! This means you did not reach this page via Cloudflare.</p>') :
      t('<p style="color:#90EE90">The Cloudflare identifier was found in the request: :identifier</p>', array(':identifier' => $cloudflare_value))
    ),
    '#suffix' => '<hr><br>',
    '#weight' => 2,
    '#cache' => array(
      'max-age' => 0,
    ),
  );

  $form['cloudflare_country_block_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('<strong>Enable Blocking</strong>'),
    '#default_value' => variable_get('cloudflare_country_block_enabled', FALSE),
    '#description' => t('Enable the blocking mechanism using the criteria below.'),
    '#weight' => 3,
  );

  module_load_include('inc', 'cloudflare_country_block', 'country_codes');

  $form['cloudflare_country_block_countries'] = array(
    '#type' => 'select',
    '#title' => t('Blocked countries'),
    '#default_value' => variable_get('cloudflare_country_block_countries', ''),
    '#options' => cloudflare_country_block_get_countries(),
    '#multiple' => TRUE,
    '#description' => t('Select the countries that should be blocked from accessing your site.<br>Select multiple by pressing Ctrl + Click.'),
    '#weight' => 4,
  );

  module_load_include('inc', 'cloudflare_country_block', 'http_response_codes');

  $http_code_options = array();
  foreach (cloudflare_country_block_get_http_codes() as $code => $info) {
    $http_code_options[$code] = $info['name'];
  }

  $form['cloudflare_country_block_response_code'] = array(
    '#type' => 'select',
    '#title' => t('HTTP Response Code'),
    '#default_value' => variable_get('cloudflare_country_block_response_code', '410'),
    '#options' => $http_code_options,
    '#description' => t("Choose the HTTP response code that should be returned if the visitor's access is being blocked.<br>This is ignored if the access denied page is being shown as exit method below."),
    '#weight' => 5,
    '#required' => TRUE,
  );

  $form['cloudflare_country_block_watchdog'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log Info to watchdog'),
    '#default_value' => variable_get('cloudflare_country_block_watchdog', TRUE),
    '#description' => t('If enabled, a watchdog info log message is written in case of a blocked access.'),
    '#weight' => 6,
  );

  $form['cloudflare_country_block_exit_method'] = array(
    '#type' => 'select',
    '#title' => t('Drupal Exit Method'),
    '#default_value' => variable_get('cloudflare_country_block_exit_method', 'drupal_exit'),
    '#options' => array(
      'drupal_exit' => t('Exit using a clean Drupal Shutdown'),
      'php_exit' => t('Exit using an immediate PHP exit'),
      'access_denied' => t('Display the Drupal access denied page'),
    ),
    '#description' => t('Choose on how your site will react to an access to be blocked.<br>Note: The access denied page does not really reduce page load from these request, because this is a fully rendered page.'),
    '#weight' => 7,
    '#required' => TRUE,
  );

  $form['cloudflare_country_block_cloudflare_missing'] = array(
    '#type' => 'select',
    '#title' => t('What if Cloudflare information is not present?'),
    '#default_value' => variable_get('cloudflare_country_block_cloudflare_missing', 'nothing'),
    '#options' => array(
      'nothing' => t('Do not validate and let them through'),
      'block' => t('Block access as if it were a match'),
    ),
    '#description' => t('When somehow a direct access is made on your server, the Cloudflare request information is not given. You may choose here on how to react in that situation.<br><strong>This is only considered if the Blocking mechanism is enabled.</strong>'),
    '#weight' => 8,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Validate the choices of the configuration form.
 *
 * @param array $form
 *   Render array of the form.
 * @param array $form_state
 *   Form State from the Drupal Form API.
 */
function cloudflare_country_block_form_validate(array $form, array &$form_state) {
  $enabled = $form_state['values']['cloudflare_country_block_enabled'];
  $countries = $form_state['values']['cloudflare_country_block_countries'];
  $response_code = $form_state['values']['cloudflare_country_block_response_code'];

  if ($enabled) {
    // If the user enabled the blocking mechanism, he has to choose at least
    // one country to be blocked.
    if (!is_array($countries) || count($countries) < 1) {
      form_set_error('cloudflare_country_block_countries', t('When the blocking mechanism is enabled, at least one country must be selected to be blocked.'));
      return;
    }
  }

  if (is_array($countries) && count($countries)) {
    module_load_include('inc', 'cloudflare_country_block', 'country_codes');
    $valid_countries = cloudflare_country_block_get_countries();
    foreach ($countries as $country_code => $value) {
      if (!array_key_exists($country_code, $valid_countries)) {
        form_set_error('cloudflare_country_block_countries', t('At least one selected country is not valid. Please select the countries from the list.'));
        return;
      }
    }
  }

  module_load_include('inc', 'cloudflare_country_block', 'http_response_codes');

  if (!array_key_exists($response_code, cloudflare_country_block_get_http_codes())) {
    form_set_error('cloudflare_country_block_response_code', t('Please select a valid HTTP response code from the list.'));
    return;
  }
}
