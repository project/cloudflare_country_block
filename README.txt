CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module helps you to block entire countries from visiting your site (and
generating massive page load), if you are using the free plan of Cloudflare
protection.

* For a full description of the module, visit the project page:
https://www.drupal.org/project/cloudflare_country_block

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/cloudflare_country_block

REQUIREMENTS
------------

This module requires no additional modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer site configuration (System module)

     Users with this permission will have access to the configuration form of
     this module.

 * Enable the blocking mechanism on the configuration form

   - Visit /admin/config/system/cloudflare-country-block

   - Check that the Cloudflare Request Information is given first!

   - Enable the blocking mechanism

   - Select which countries should be banned from your site.

   - Select reponse codes, logging details and exit behavior.

   - Choose the behavior when the Cloudflare request information is not given
     (e.g. on an unauthorized direct access). CAUTION: If you do not have the
     information in the request and choose to block accesses that do not
     provide this, you may lock yourself out!). The user 1 (super
     administrator) can not be locked out.

TROUBLESHOOTING
---------------

 * I locked myself out and cannot regain control

   - You can use the user 1 to visit the administration page and change the
     configuration

   - You can use drush to disable blocking:
     drush vset cloudflare_country_block_enabled FALSE

FAQ
---

Q: I changed Nameservers to Cloudflare and blocked a country, but am still
   getting page views from that particular country.

A: Note that a Nameserver propagation can take up to 48 hours to be completed
   on the entire world. If you freshly switch to Cloudflare during an attack,
   it could be that the attackers know your direct IP and won't go through
   Cloudflare. Use the "What if Cloudflare information is not present" option
   to ban those requests.

MAINTAINERS
-----------

Current maintainers:
 * Florian Müller (fmueller_previon) - https://www.drupal.org/user/3524567

This project has been sponsored by:
 * Previon Plus AG
   Previon+ is specialized in digital services & platforms and supports
   its clients on finding suitable ideas, developing digital business models
   and processes and accompanies them on their digital experience.
   Visit https://www.previon.ch for more information.
